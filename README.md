# 3itTask

> tarea asignada  para postular
> librería nativa que requirió  configuracion nativa en ios y android react-native-svg 

## Pasos   para usar 

### instalar dependecias 
>- yarn install  

### pra correr en Ios simulator
> - ir a [ cd ios] 
> - eliminar [Podfile.lock ]
> - hacer un [pod install] 
> - salir a principal [cd ..]
> - poner [yarn ios ] 

### pra correr en Android simulator
>- ejecutar lo siguiente
````export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
````
>- luego [react-native run-android]

q