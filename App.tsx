
import React from 'react';
import { LogBox } from 'react-native';
import NavigatorMain from './app/configs/navigator/index' 




const App = () => {
  return (
    <NavigatorMain />
  )
};


export default App;

LogBox.ignoreLogs([
  'RCTBridge required dispatch_sync to load RCTDevLoadingView. This may lead to deadlocks',
]);
