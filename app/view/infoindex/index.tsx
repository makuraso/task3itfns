
import React, { useEffect } from 'react';
import { Text, Button, H1, Icon } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import { StylesMainContainer } from './styles/index'
import { NsPInfoindexView } from './index.td';
import { MyGeneralLoader } from '../../helpers/mygeneralloader';
import { CONSTANS } from '../../configs/CONSTANS';
import { navReponsive } from '../../helpers/screen';
import { LineChart, YAxis, Grid as GridG } from 'react-native-svg-charts'
import { View } from 'react-native';


export const InfoindexView = ({ navigation, componentState, itemDEtail }: NsPInfoindexView.Props): JSX.Element => {
    const data = componentState.payload?.serie.map(item=>item.valor).slice(0, 10);

    const contentInset = { top: 20, bottom: 20 }

    const mainRender = (): JSX.Element => {
        return (

            <Grid style={{ ...StylesMainContainer.container }}>
                <Row size={50} style={{ backgroundColor: 'white' }}>
                    <Col>
                        <Row size={30} style={{ backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: navReponsive(30, 37, 40, 55), fontWeight: '600',color:CONSTANS.BASE_COLORS.PRIMARY }}>$ {componentState.payload?.serie[0].valor}</Text>
                        </Row>
                        <Row size={23} style={{ backgroundColor: 'white' }}>
                            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: navReponsive(14, 15, 16, 33)}} >Nombre</Text>
                            </Col>
                            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: navReponsive(11, 12, 13, 29)}}>{itemDEtail.nombre}</Text>
                            </Col>
                        </Row>
                        <Row size={23} style={{ backgroundColor: 'white' }}>
                            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: navReponsive(14, 15, 16, 33)}} >Fecha</Text>
                            </Col>
                            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: navReponsive(11, 12, 13, 29)}}>{new Date(componentState.payload?.serie[0].fecha as string).toLocaleDateString()}</Text>
                            </Col>
                        </Row>
                        <Row size={23} style={{ backgroundColor: 'white' }}>
                            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: navReponsive(14, 15, 16, 33)}} >Unidad de medida</Text>
                            </Col>
                            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: navReponsive(11, 12, 13, 29)}}>{itemDEtail.unidad_medida}</Text>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row size={50} style={{ backgroundColor: '#b0adac',justifyContent:'center',alignItems:'center' }}>
                    <View style={{ height: '90%', flexDirection: 'row',width:'90%',backgroundColor:CONSTANS.BASE_COLORS.PRIMARY }}>
                        <YAxis
                            data={data}
                            contentInset={contentInset}
                            svg={{
                                fill: 'white',
                                fontSize: 10 
                            }}
                            numberOfTicks={10}
                            formatLabel={(value) => `${value}`}
                            
                        />
                        <LineChart
                            style={{ flex: 1, marginLeft: 16}}
                            data={data}
                            svg={{ stroke: 'white', }}
                            contentInset={contentInset}
                        >
                            <GridG />
                        </LineChart>
                    </View>
                </Row>
            </Grid>
        );
    }

    switch (componentState.event) {
        case 'FETCHING_INDICATORS':
            return (
                <MyGeneralLoader
                    ColorSpinner='white'
                    visible={true}
                    backgroundColor={CONSTANS.BASE_COLORS.PRIMARY}
                    isOverlay={true}
                    spinnerSize='small'
                    textSize={navReponsive(11, 12, 13.5, 25)}
                    textMessage='Obteniendo indicadores ...'
                    type='fade'
                />
            )

        default:
            return mainRender();
    }

};
