  
import { NSP_InfoindexComponent } from "../../component/infoindex/index.td";
import { Navigation } from "../../configs/navigator/all-routes";

export namespace NsPInfoindexView{
    /**
     * Props
     */
    export type Props ={
        navigation:Navigation,
        componentState: NSP_InfoindexComponent.ComponentState
        itemDEtail:NSP_InfoindexComponent.ItemIdex
      
    }
};
