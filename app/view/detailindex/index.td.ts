 
import { NSP_DetailindexComponent } from "../../component/detailindex/index.td";
import { Navigation } from "../../configs/navigator/all-routes";

export namespace NsPDetailindexView{
    /**
     * Props
     */
    export type Props ={
        navigation:Navigation,
        componentState:NSP_DetailindexComponent.ComponentState
    }
};
