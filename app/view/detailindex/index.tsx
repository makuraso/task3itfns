
import React, { useEffect } from 'react';
import { Text, Button, H1, Icon } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import { StylesMainContainer } from './styles/index'
import { NsPDetailindexView } from './index.td';
import { MyGeneralLoader } from '../../helpers/mygeneralloader';
import { navReponsive } from '../../helpers/screen';
import { Dimensions, ScrollView, View } from 'react-native';
import { CONSTANS } from '../../configs/CONSTANS';


export const DetailindexView = ({ navigation, componentState }: NsPDetailindexView.Props): JSX.Element => {


    const mainRender = (): JSX.Element => {
        return (

            <Grid style={{ ...StylesMainContainer.container }}>
                <Col>
                    <ScrollView contentContainerStyle={{ width: Dimensions.get('screen').width, minHeight: '100%', paddingBottom: '5%' }}>
                        {
                            componentState.payload?.serie.map(item => {
                                return (
                                    <View key={`9389484jfjjsdfgdsa_${item.valor}`} style={{ backgroundColor: 'white', borderTopWidth: 1, borderColor: '#ebedf0', width: '100%', height: 50 }}>
                                        <Grid>
                                            <Row >
                                                <Col style={{justifyContent:'center',alignItems:'center'}}>
                                                    <Text style={{fontWeight: '500', fontSize: navReponsive(12, 13, 14.5, 21), fontStyle: 'italic', color: CONSTANS.BASE_COLORS.PRIMARY}}>{new Date(item.fecha).toLocaleDateString()}</Text>
                                                </Col>
                                                <Col style={{justifyContent:'center',alignItems:'center'}}>
                                                    <Text style={{fontWeight: '600', fontSize: navReponsive(12, 13, 14.5, 21), fontStyle: 'italic', color:'black'}}>$ {item.valor}</Text>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </View>
                                )
                            })
                        }
                    </ScrollView>
                </Col>

            </Grid>
        );
    }
    switch (componentState.event) {
        case 'FETCHING_INDICATORS':
            return (
                <MyGeneralLoader
                    ColorSpinner='white'
                    visible={true}
                    backgroundColor={CONSTANS.BASE_COLORS.PRIMARY}
                    isOverlay={true}
                    spinnerSize='small'
                    textSize={navReponsive(11, 12, 13.5, 25)}
                    textMessage='Obteniendo indicadores ...'
                    type='fade'
                />
            )

        default:
            return mainRender();
    }
};
