import { NSP_DashboardComponent } from "../../component/dashboard/index.td";
import { Navigation } from "../../configs/navigator/all-routes";

export namespace NsPDashboardView {
    /**
     * Props
     */
    export type Props = {
        navigation: Navigation,
        componentState: NSP_DashboardComponent.ComponentState

    }
    
    /**
     * es para mostrar los mensajes principales del dashboard
     */
    export type MainRender = () => JSX.Element;
};
