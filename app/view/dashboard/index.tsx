
import React, { version } from 'react';
import { Text } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import { StylesMainContainer } from './styles/index'
import { NsPDashboardView } from './index.td';
import { MyGeneralLoader } from './../../helpers/mygeneralloader';
import { CONSTANS } from '../../configs/CONSTANS';
import { navReponsive } from '../../helpers/screen';
import { Icon } from 'native-base';
import { Dimensions, ScrollView } from 'react-native';

export const DashboardView = ({
    navigation,
    componentState
}: NsPDashboardView.Props): JSX.Element => {

    const keys = [
        'uf',
        'ivp',
        'dolar',
        'dolar_intercambio',
        'euro',
        'ipc',
        'utm',
        'imacec',
        'tpm',
        'libra_cobre',
        'tasa_desempleo',
        'bitcoin'];

    const mainRender: NsPDashboardView.MainRender = (): JSX.Element => {
        return (

            <Grid style={{ ...StylesMainContainer.container }}>
                <Col>
                    <ScrollView contentContainerStyle={{ width: Dimensions.get('screen').width, minHeight: '100%', paddingBottom: '5%' }}>
                        <Row size={8.33} style={{ backgroundColor: 'white' }}>
                            <Grid>
                                {
                                    keys.map(item => {
                                        return (
                                            <Row key={`9389484jfjjsdfgdsa_${item}`} size={8.33} style={{ backgroundColor: 'white', borderTopWidth: 1, borderColor: '#ebedf0' }}>
                                                <Col onPress={() => {
                                                    navigation.navigate('detailindex',{indexSelected:componentState.payload[item]});
                                                }}>
                                                    <Row size={45} style={{ justifyContent: 'flex-start', alignItems: 'center' }}><Text style={{ fontWeight: '300', fontSize: navReponsive(14, 15, 16, 23), marginLeft: '2%' }}>{componentState.payload[item].nombre}</Text></Row>
                                                    <Row size={55}>
                                                        <Col size={80} style={{ justifyContent: 'center', alignItems: 'flex-start' }}><Text style={{ fontWeight: '500', fontSize: navReponsive(12, 13, 14.5, 21), fontStyle: 'italic', color: CONSTANS.BASE_COLORS.PRIMARY, marginLeft: '2%' }}>{componentState.payload[item].unidad_medida}</Text></Col>
                                                        <Col onPress={() => { navigation.navigate('infoindex',{indexSelected:componentState.payload[item]});}} size={10} style={{ justifyContent: 'center', alignItems: 'center' }}><Icon type='AntDesign' name='infocirlceo' style={{ fontSize: navReponsive(14, 15, 16, 23), color: CONSTANS.BASE_COLORS.PRIMARY }} /></Col>
                                                        <Col size={10} style={{ justifyContent: 'center', alignItems: 'center' }}><Icon type='MaterialIcons' name='keyboard-arrow-right' style={{ fontSize: navReponsive(20, 22, 21, 28), color: '#c7c8c9' }} /></Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        )
                                    })
                                }
                            </Grid>
                        </Row>
                    </ScrollView>
                </Col>

            </Grid>
        );
    }
    switch (componentState.event) {
        case 'FETCHING_INDICATORS':
            return (
                <MyGeneralLoader
                    ColorSpinner='white'
                    visible={true}
                    backgroundColor={CONSTANS.BASE_COLORS.PRIMARY}
                    isOverlay={true}
                    spinnerSize='small'
                    textSize={navReponsive(11, 12, 13.5, 25)}
                    textMessage='Obteniendo indicadores ...'
                    type='fade'
                />
            )

        default:
            return mainRender();
    }


};
