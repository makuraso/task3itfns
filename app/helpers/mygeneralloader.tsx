import { Col, Row, Grid } from "react-native-easy-grid";
import { Spinner, Text, Icon } from 'native-base';
import { getWindowsSize } from './../helpers/screen';
import { StyleSheet, Modal } from 'react-native';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';


interface PropsTypes {
    backgroundColor: string,
    spinnerSize: number | 'small' | 'large',
    ColorSpinner: string,
    textSize: number,
    textMessage: string,
    isOverlay: boolean,
    visible: boolean,
    type:'none' | 'slide' | 'fade'
}
export const MyGeneralLoader = (props: PropsTypes): JSX.Element => {
    try { 
        if ((props.hasOwnProperty('isOverlay'))&& props.isOverlay) {
            return (
                <Modal
                    animationType={props.type}
                    transparent={true}
                    visible={props.visible}
                    onRequestClose={() => {
                    }}
                >
                    <Grid>
                        <Col style={{ backgroundColor: props.backgroundColor }}>
                            <Row style={{
                                justifyContent: 'center',
                                alignItems: 'flex-end',
                            }}>
                                <Spinner size={props.spinnerSize} color={props.ColorSpinner} />
                            </Row>
                            <Row style={{
                                justifyContent: 'center',
                                alignItems: 'flex-start',
                            }}>
                                <Text style={{
                                    color: 'white',
                                    fontSize: getWindowsSize({ type: 'w', size: props.textSize })
                                }}> {props.textMessage}</Text>
                            </Row>
                        </Col>
                    </Grid>
                </Modal>
            )
        }
        return (
            <Grid>
                <Col style={{ backgroundColor: props.backgroundColor }}>
                    <Row style={{
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                    }}>
                        <Spinner size={props.spinnerSize} color={props.ColorSpinner} />
                    </Row>
                    <Row style={{
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                    }}>
                        <Text style={{
                            color: 'white',
                            fontSize: getWindowsSize({ type: 'w', size: props.textSize })
                        }}> {props.textMessage}</Text>
                    </Row>
                </Col>
            </Grid>
        )
    } catch (error) {
        throw error;
    }
}




MyGeneralLoader.propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    spinnerSize: PropTypes.any.isRequired,
    ColorSpinner: PropTypes.string.isRequired,
    textSize: PropTypes.number.isRequired,
    textMessage: PropTypes.string.isRequired
}



export const StylesMainContainer = StyleSheet.create({

});