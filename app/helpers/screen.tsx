import { Dimensions } from "react-native";
const dimension = Dimensions.get('window');
export const imageHeight = () => {
    return Math.round(dimension.width * 9 / 16);
}

export const imageWidth = (): number => {
    return dimension.width;
}
type Typos = 'h' | 'w';

interface PropsSizeWindows {
    type: Typos,
    size: number
}

export const getWindowsSize = (props: PropsSizeWindows): number => {
    try {
        switch (props.type) {
            case 'w':
                return (Dimensions.get('window').width - (Dimensions.get('window').width - props.size));
            case 'h':
                return (Dimensions.get('window').height - (Dimensions.get('window').height - props.size));
            default:
                throw 'Not compatible size';
        }
    } catch (error) {
        throw 'Not compatible size';
    }
}


export const navReponsive = (small: any, medium: any, large: any, xlarge: any) => {
    if (Dimensions.get('window').height >= 0 && Dimensions.get('window').height < 600) {
        return small;
    }
    if (Dimensions.get('window').height > 600 && Dimensions.get('window').height < 800) {
        return medium;
    }
    if (Dimensions.get('window').height > 800 && Dimensions.get('window').height < 900) {
        return large;
    }
    if (Dimensions.get('window').height > 900) {
        return xlarge;
    }
}


export const textEraseAndCut = (text: string,constaint:number): string => {
    try {
        if (text && typeof text === 'string') {
            if (text.length > constaint) {
                return `${(text.slice(0, constaint))}...`
            }
            if (text.length <= constaint) {
                return text;
            }
            return text;
        }
        throw 'generateCutTextLeng() typeof nil or empty input'
    } catch (error) {
        throw error;
    } 
}
