import { AxiosResponse } from "axios";

export namespace RequestCentralApi {

    export type Url = {
        [key: string]: string
    }

    /**
     * obtengo todos los indicadores  economicos actuales  para la api  versión 1.6.0
     */
    export type GetEconomicIndicators = () => Promise<AxiosResponse<PayloadGetEconomicIndicators>>

    /**
     * payload para version 1.6.0 
     */
    export type PayloadGetEconomicIndicators = {
        version: string,
        autor: 'mindicador.cl',
        fecha: string
        uf: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        },
        ivp: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        },
        dolar: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        },
        dolar_intercambio: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        },
        euro: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        },
        ipc: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        },
        utm: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        },
        imacec: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        },
        tpm: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        },
        libra_cobre: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        },
        tasa_desempleo: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        },
        bitcoin: {
            codigo: string,
            nombre: string,
            unidad_medida: string
            fecha: string
            valor: number
        }
    }

    export type PayloadGetDetailIndexApi = {
        version: string,
        autor: string,
        codigo: string,
        nombre: string,
        unidad_medida: string,
        serie: Array<{
            fecha: string,
            valor: number
        }>
    }
    export type GetDetailIndexApi = (codeIndex: string) => Promise<AxiosResponse<PayloadGetDetailIndexApi>>
}