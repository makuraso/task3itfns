import { useGet } from './../api/get';
import { usePost } from './../api/post';
import { RequestCentralApi } from './index.td';
import { AxiosResponse } from 'axios';

export const useRequestApi = () => {

    let stage = 'local';

    let { get } = useGet();
    let { post } = usePost();

    const getEconomicIndicators: RequestCentralApi.GetEconomicIndicators = async () => {
        let url: RequestCentralApi.Url = {
            local: 'https://mindicador.cl/api',
            production: ''
        }

        let data = await get(
            url[stage]
        ) as AxiosResponse<RequestCentralApi.PayloadGetEconomicIndicators>;
        return data;
    }

    const getDetailIndexApi: RequestCentralApi.GetDetailIndexApi = async (codeIndex:string) => {
        let url: RequestCentralApi.Url = {
            local: `https://mindicador.cl/api/${codeIndex}`,
            production: ''
        }

        let data = await get(
            url[stage]
        ) as AxiosResponse<RequestCentralApi.PayloadGetDetailIndexApi>;
        return data;
    }


    return {
        getEconomicIndicators,
        getDetailIndexApi
    }
}