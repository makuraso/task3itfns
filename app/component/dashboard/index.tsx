
import React, { useEffect, useState } from 'react';
import { NSP_DashboardComponent } from './index.td';
import { DashboardView } from './../../view/dashboard/index'
import { CONSTANS } from './../../configs/CONSTANS';
import { Dimensions, TouchableOpacity, View } from 'react-native';
import { Icon } from 'native-base';
import { useRequestApi } from './../../requestCentral/request';

export const DashboardComponent = ({ navigation }: NSP_DashboardComponent.Props): JSX.Element => {

    const { getEconomicIndicators } = useRequestApi();

    const [componentState, setComponentState] = useState<NSP_DashboardComponent.ComponentState>({
        event: 'FETCHING_INDICATORS',
        payload: null
    });


    useEffect(() => {
        getEconomicData();
        return () => {
            _clearState();
        }
    }, []);

    /**Reset All */
    const _clearState = () => {
        try {
            setComponentState({
                event: 'FETCHING_INDICATORS',
                payload: null
            })
        } catch (error) {
            throw error;
        }
    }

    const getEconomicData: NSP_DashboardComponent.GetEconomicData = async () => {
        try {

            let { data } = await getEconomicIndicators();

            setComponentState((prev) => ({
                ...prev,
                event: 'MAIN',
                payload: data
            }));

        } catch (error) {
            // aquñi usaría un hoc global de control de exception en tiempo real si me contrataran :)
            console.log(error)
        }
    }

    return (
        <DashboardView
            navigation={navigation}
            componentState={componentState}
        />
    );
};

DashboardComponent.navigationOptions = (screenProps: any) => {
    return ({
        title: screenProps.navigation.getParam("yourParam", 'Indicadores'),
        headerStyle: {
            backgroundColor: CONSTANS.NAV_HEADER.BACKGROUND,
            height: CONSTANS.NAV_HEADER.HEIGHT,
            ...CONSTANS.NAV_HEADER.SHADOWN
        },
        headerLeft: () => (
            <TouchableOpacity disabled={screenProps.navigation.getParam('loadingFetch')}
                onPress={async () => {
                    // screenProps.navigation.openDrawer();
                }}>
                <Icon
                    style={{ marginLeft: (Dimensions.get('window').width - (Dimensions.get('window').width - 15)), color: 'white' }}
                    type='MaterialCommunityIcons'
                    name='menu'
                />
            </TouchableOpacity>
        ),
        headerRight: () => (
            <View
                style={{
                    width: 120,
                    height: '100%',
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}
            >
                {/* NOTIFICATIONS BUTTON */}
                <View style={{ width: '33.33%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    {/* <TouchableOpacity>
                        <Icon
                            type='Ionicons'
                            name='ios-notifications-outline'
                            style={{
                                color: 'white',
                                alignSelf: 'center',
                                fontSize: CONSTANS.NAV_HEADER.ICONS_SIZE
                            }}
                        />
                    </TouchableOpacity> */}
                </View>

                {/* BUTTON SEARCH TEXT */}
                <View style={{ width: '33.33%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    {/* <TouchableOpacity
                        onPress={() => {
                          
                        }}
                    >
                        <Icon
                            type='Ionicons'
                            name='ios-notifications-outline'
                            style={{
                                color: 'white',
                                alignSelf: 'center',
                                fontSize: CONSTANS.NAV_HEADER.ICONS_SIZE
                            }}
                        />
                    </TouchableOpacity> */}
                </View>

                {/* FILTRO TIPOS */}
                <View style={{ width: '33.33%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => {
                            // screenProps.navigation.getParam('setEventRender')((prev: EvensRenders) => ({ ...prev, event: 'FILTER_DASH' }));
                        }}
                    >
                        <Icon
                            type='Entypo'
                            name='dots-three-vertical'
                            style={{
                                color: 'white',
                                fontSize: CONSTANS.NAV_HEADER.ICONS_SIZE
                            }}
                        />
                    </TouchableOpacity>
                </View>
            </View>

        ),
        headerShown: true,
        // title: 'Inicio',
        headerTitleStyle: {
            color: CONSTANS.NAV_HEADER.COLOR_TEXT,
            fontSize: CONSTANS.NAV_HEADER.SIZE_TITLE
        },
        headerTitleAlign: 'left'
    })
};
