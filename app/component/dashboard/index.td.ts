
import { RequestCentralApi } from '../../requestCentral/index.td';
import { Navigation } from './../../configs/navigator/all-routes/index';

export namespace NSP_DashboardComponent {

    /**
     * Props component
     */
    export type Props = {
        navigation: Navigation
    }

    export type keys = 'version' |
        'autor' |
        'fecha' |
        'uf' |
        'ivp' |
        'dolar' |
        'dolar_intercambio' |
        'euro' |
        'ipc' |
        'utm' |
        'imacec' |
        'tpm' |
        'libra_cobre' |
        'tasa_desempleo' |
        'bitcoin';

    type OptionRequirements = Record<keys, RequestCentralApi.PayloadGetEconomicIndicators>

    export type ComponentState = {
        event: 'MAIN' | 'FETCHING_INDICATORS',
        payload: RequestCentralApi.PayloadGetEconomicIndicators | null,
    }

    


    export type GetEconomicData = () => Promise<void>

};

