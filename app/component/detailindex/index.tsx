
import React, { useEffect, useState } from 'react';
import { NSP_DetailindexComponent } from './index.td';
import { DetailindexView } from './../../view/detailindex/index'
import { CONSTANS } from '../../configs/CONSTANS';
import { Dimensions, TouchableOpacity, View } from 'react-native';
import { Icon } from 'native-base'; 
import { useRequestApi } from './../../requestCentral/request';

export const DetailindexComponent = ({ navigation }: NSP_DetailindexComponent.Props): JSX.Element => {

    const { getDetailIndexApi } = useRequestApi();

    const [itemDEtail, setItemDetail] = useState<ItemIdex>(navigation.getParam('indexSelected') as any);

    const [componentState, setComponentState] = useState<NSP_DetailindexComponent.ComponentState>({
        event: 'FETCHING_INDICATORS',
        payload: null
    });

    useEffect(() => {
        getDetailIndex();
        return () => {
            _clearState();
        }
    }, []);

    /**Reset All */
    const _clearState = () => {
        try {
            setComponentState({
                event: 'FETCHING_INDICATORS',
                payload: null
            })
        } catch (error) {
            throw error;
        }
    }

    const getDetailIndex: NSP_DetailindexComponent.GetDetailIndex = async () => {
        try {
            let { data } = await getDetailIndexApi(itemDEtail.codigo);
            console.log(data)
            setComponentState((prev) => ({
                ...prev,
                event: 'MAIN',
                payload: data
            }));
        } catch (error) {
            throw error;
        }
    }

    return (
        <DetailindexView
            navigation={navigation}
            componentState={componentState}
        />
    );
};


DetailindexComponent.navigationOptions = (screenProps: any) => {
    return ({
        title: screenProps.navigation.getParam('indexSelected').nombre,
        headerStyle: {
            backgroundColor: CONSTANS.NAV_HEADER.BACKGROUND,
            height: CONSTANS.NAV_HEADER.HEIGHT,
            ...CONSTANS.NAV_HEADER.SHADOWN
        },
        headerLeft: () => (
            <TouchableOpacity disabled={false}
                onPress={async () => {
                    screenProps.navigation.pop();
                }}>
                <Icon
                    style={{ marginLeft: (Dimensions.get('window').width - (Dimensions.get('window').width - 15)), color: 'white' }}
                    type='AntDesign'
                    name='arrowleft'
                />
            </TouchableOpacity>
        ),
        headerRight: () => (
            <View
                style={{
                    width: 120,
                    height: '100%',
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}
            >
                {/* NOTIFICATIONS BUTTON */}
                <View style={{ width: '33.33%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    {/* <TouchableOpacity>
                        <Icon
                            type='Ionicons'
                            name='ios-notifications-outline'
                            style={{
                                color: 'white',
                                alignSelf: 'center',
                                fontSize: CONSTANS.NAV_HEADER.ICONS_SIZE
                            }}
                        />
                    </TouchableOpacity> */}
                </View>

                {/* BUTTON SEARCH TEXT */}
                <View style={{ width: '33.33%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    {/* <TouchableOpacity
                        onPress={() => {
                          
                        }}
                    >
                        <Icon
                            type='Ionicons'
                            name='ios-notifications-outline'
                            style={{
                                color: 'white',
                                alignSelf: 'center',
                                fontSize: CONSTANS.NAV_HEADER.ICONS_SIZE
                            }}
                        />
                    </TouchableOpacity> */}
                </View>

                {/* FILTRO TIPOS */}
                <View style={{ width: '33.33%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => {
                            // screenProps.navigation.getParam('setEventRender')((prev: EvensRenders) => ({ ...prev, event: 'FILTER_DASH' }));
                        }}
                    >
                        <Icon
                            type='Entypo'
                            name='dots-three-vertical'
                            style={{
                                color: 'white',
                                fontSize: CONSTANS.NAV_HEADER.ICONS_SIZE
                            }}
                        />
                    </TouchableOpacity>
                </View>
            </View>

        ),
        headerShown: true,
        // title: 'Inicio',
        headerTitleStyle: {
            color: CONSTANS.NAV_HEADER.COLOR_TEXT,
            fontSize: CONSTANS.NAV_HEADER.SIZE_TITLE
        },
        headerTitleAlign: 'left'
    })
};



type ItemIdex = {
    "codigo": string,
    "nombre": string,
    "unidad_medida": string,
    "fecha": string,
    "valor": number
}
