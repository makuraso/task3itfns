 
import { RequestCentralApi } from '../../requestCentral/index.td';
import {Navigation} from './../../configs/navigator/all-routes/index';

export namespace NSP_InfoindexComponent { 
 
    /**
     * Props component
     */
    export type Props = {
        navigation:Navigation
    }

    export type GetDetailIndex = () => Promise<void>

    

    export type ItemIdex = {
        "codigo": string,
        "nombre": string,
        "unidad_medida": string,
        "fecha": string,
        "valor": number
    }
    export type ComponentState = {
        event: 'MAIN' | 'FETCHING_INDICATORS',
        payload: RequestCentralApi.PayloadGetDetailIndexApi | null,
    }
}; 

