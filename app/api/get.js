import Axios from 'axios';
import React, { useEffect, useState } from 'react';

export const useGet = () => {
    const [_getIsWorking, _setGetIsWorking] = useState(false);

    const get = async (_path,_params = null,auth0 = '') => {
        try {
            let resp;
            let config = {
                headers: {
                  'Content-Type': 'application/json;charset=UTF-8',
                  'Access-Control-Allow-Origin': '*',
                  'Authorization': auth0
                },
                params:_params
              };
            _setGetIsWorking(true);
            resp = await Axios.get(_path,config);
            _setGetIsWorking(false);
            return resp;
        } catch (error) {
            if (error.response.status !== 401) {
                throw error;
            }
        }
    }

    return {
        get,
        _getIsWorking
    }
}