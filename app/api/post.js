import { useState, useEffect } from 'react';
import Axios from 'axios';

export const usePost = () => {
    const [isLoadingPostService, _setIsLoadingPostService] = useState(false);
    const [dataPostServiceResponse, _setDataPostServiceResponse] = useState(null);
    const [errorPostService, _setErrorPostService] = useState(null);

    /**
     * 
     * @param {Object} [_body]
     * @param {String} _url
     * @param {String} [_token]
     */
    const post = async (_body, _url, _token) => {
        try {

            _setIsLoadingPostService(true);
            let postData = _body;
            let config = {
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8',
                    'Access-Control-Allow-Origin': '*'
                }
            };
            if (_token) {
                config.headers.Authorization = _token;
                console.log(config);
            }

            let data = await Axios.post(
                _url,
                postData,
                //config
            );
            _setDataPostServiceResponse(data);
            _setIsLoadingPostService(false);

            return data;
        } catch (error) {
 
            _setErrorPostService(error);
            _setIsLoadingPostService(false); 
            throw error;
        }
    }

    const postMultipart = async (_body, _url, _token) => {
        try {

            _setIsLoadingPostService(true);
            let postData = _body;
            let config = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Access-Control-Allow-Origin': '*'
                }
            };
            if (_token) {
                config.headers.Authorization = _token;
                console.log(config);
            }

            let data = await Axios.post(
                _url,
                postData,
                config
            ); 
            _setDataPostServiceResponse(data);
            _setIsLoadingPostService(false);

            return data;
        } catch (error) {
            _setErrorPostService(error);
            _setIsLoadingPostService(false);
            if (error.response.status === 401) {
                return;
            }
            throw error;
        }
    }

    useEffect(() => {
        return () => {
            _setIsLoadingPostService(false);
            _setDataPostServiceResponse(null);
            _setErrorPostService(null);
        }
    }, []);

    return {
        dataPostServiceResponse,
        errorPostService,
        isLoadingPostService,
        post,
        postMultipart
    };
};