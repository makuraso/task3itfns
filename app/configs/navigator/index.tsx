import { NavigationNavigator, NavigationProp, NavigationState, createAppContainer, createSwitchNavigator } from "react-navigation"; 
import {dashBoardStack} from './dashboard/index';

const AppSwitchNavigator: NavigationNavigator<{}, NavigationProp<NavigationState>> = createSwitchNavigator({
    zero: { screen: dashBoardStack }, 
});

export default createAppContainer(AppSwitchNavigator);