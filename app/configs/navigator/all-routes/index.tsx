
interface AllRoutes {
    init:string
    dashboard:string
    detailindex:string
    infoindex:string
       
}

export type AllowedRoutes =   'init'  | 'dashboard'  | 'detailindex'  | 'infoindex' 

export interface Navigation {
    navigate(routeName: AllowedRoutes,params?:object | Array<object> | []): void,
    pop(): void,
    setParams(params:object):void
    getParam(key:string,notFountDefault?:string):object | Array<object> | [] | undefined

}

// rutas reales
export const Routes: AllRoutes = {
    init:'init',
    dashboard:'dashboard',
    detailindex:'detailindex',
    infoindex:'infoindex',
    
}
