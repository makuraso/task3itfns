import { createStackNavigator } from "react-navigation-stack"; 
import { NavigationNavigator, NavigationProp, NavigationState } from 'react-navigation';
import { Routes } from './../all-routes/index';  
import {DashboardComponent} from './../../../component/dashboard';
import {DetailindexComponent} from './../../../component/detailindex';
import {InfoindexComponent} from './../../../component/infoindex';

export const dashBoardStack: NavigationNavigator<any, NavigationProp<NavigationState>> = createStackNavigator(
    {
        [Routes.dashboard]: {
            screen: DashboardComponent 
        },
        [Routes.detailindex]: {
            screen: DetailindexComponent 
        },
        [Routes.infoindex]: {
            screen: InfoindexComponent 
        },
    }as any
);
