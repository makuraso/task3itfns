import { navReponsive } from './../../app/helpers/screen';


export const CONSTANS = {
  /**
   * Colores base para toda la aplicación 
   */
  BASE_COLORS: {
    PRIMARY: '#1376d9',//'#CE2402',
    SECONDARY: 'white',
    BASE_SHADOWN: {
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.30,
      shadowRadius: 4.65,

      elevation: 8,
    }
  },
  /**
   * Configuración base para todos los headers de la app
   */
  NAV_HEADER: {
    BACKGROUND: '#1376d9',
    COLOR_TEXT: 'white',
    FONT_FAMILY: 'Oswald-SemiBold',
    SIZE_TITLE: navReponsive(15, 15, 15, 15),
    HEIGHT: navReponsive(60, 65, 85, 90),
    ICONS_SIZE: navReponsive(15, 15, 20, 30),
    ICON_BACK_OPTIONS: {
      ICONS_SIZE_BACK: navReponsive(17, 18, 18, 22),
      TYPE: 'SimpleLineIcons' as "AntDesign" | "Entypo" | "EvilIcons" | "Feather" | "FontAwesome" | "FontAwesome5" | "Foundation" | "Ionicons" | "MaterialCommunityIcons" | "MaterialIcons" | "Octicons" | "SimpleLineIcons" | "Zocial",
      NAME: 'arrow-left'
    },
    SHADOWN: {
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.30,
      shadowRadius: 4.65,

      elevation: 8,
    }
  },
  BASE_INPUT:{
    ERROR_TEXT:{
      SIZE:navReponsive(11, 11, 13, 15)
    },
    ICONS_SIZES:navReponsive(13, 12, 15, 23)
  },
  BASE_FONTS:{
    OswwaldBold:'Oswald-Bold',
    OswwaldExtraLight:'Oswald-ExtraLight',
    OswwaldLight:'Oswald-Light',
    OswwaldMedium:'Oswald-Medium',
    OswwaldRegular:'Oswald-Regular',
    OswwaldSemiBold:'Oswald-SemiBold',
  }
}